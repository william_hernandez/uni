package com.unipay.univ3.models;

public class Transaccion {

    public String phoneNumberFrom;
    public String phoneNumberTo;
    public String concept;
    public String createDate;
    public double balance;

    public Transaccion(String phoneNumberFrom, String phoneNumberTo, String concept, double balance, String createDate) {
        this.phoneNumberFrom = phoneNumberFrom;
        this.phoneNumberTo = phoneNumberTo;
        this.concept = concept;
        this.balance = balance;
        this.createDate = createDate;
    }

    public Transaccion() {

    }

    public String getPhoneNumberFrom() {
        return phoneNumberFrom;
    }

    public void setPhoneNumberFrom(String phoneNumberFrom) {
        this.phoneNumberFrom = phoneNumberFrom;
    }

    public String getPhoneNumberTo() {
        return phoneNumberTo;
    }

    public void setPhoneNumberTo(String phoneNumberTo) {
        this.phoneNumberTo = phoneNumberTo;
    }

    public String getConcept() {
        return concept;
    }

    public void setConcept(String concept) {
        this.concept = concept;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreatedate(String createDate) {
        this.createDate = createDate;
    }
}
