package com.unipay.univ3.ui.resumen;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.unipay.univ3.R;
import com.unipay.univ3.models.Transaccion;
import com.unipay.univ3.ui.adapters.ListaTransaccionesAdapter;
import com.unipay.univ3.utilidades.Settings_VAR;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class HistorialTransacciones extends Fragment {

    ArrayList<String> lista = null;
    ArrayList<Transaccion> listaTransacciones;

    private RecyclerView recyclerView;
    private ListaTransaccionesAdapter transaccionAdapter;

    public HistorialTransacciones() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_historial_transacciones, container, false);

        recyclerView = root.findViewById(R.id.rvTransacciones);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        recibirAllTransacciones();
        return root;
    }

    private void recibirAllTransacciones(){
        RequestQueue queue = Volley.newRequestQueue(getContext());
        listaTransacciones = new ArrayList<Transaccion>();
        lista = new ArrayList<String>();
        String urlAll = Settings_VAR.URL_consultarAllTransaccion;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, urlAll, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray respuestaJSOn = new JSONArray(response.toString());
                    int totalEnct = respuestaJSOn.length();

                    Transaccion objTransaccion = null;
                    for (int i = 0; i < respuestaJSOn.length(); i++){
                        JSONObject transaccion = respuestaJSOn.getJSONObject(i);
                        String phoneNumberFrom = transaccion.getString("phoneNumberFrom");
                        String phoneNumberTo = transaccion.getString("phoneNumberTo");
                        String concept = transaccion.getString("concept");
                        double balance = transaccion.getDouble("balance");
                        String createDate = transaccion.getString("createDate");

                        objTransaccion = new Transaccion(phoneNumberFrom, phoneNumberTo, concept, balance, createDate);

                        listaTransacciones.add(objTransaccion);

                        transaccionAdapter = new ListaTransaccionesAdapter(getContext(), listaTransacciones);

                        recyclerView.setAdapter(transaccionAdapter);

                        Log.i("Id:    ", String.valueOf(objTransaccion.getPhoneNumberFrom()));
                        Log.i("Nombre:    ", String.valueOf(objTransaccion.getPhoneNumberTo()));

                    }

                } catch (JSONException ex){
                    String none = ex.toString();
                    Log.i("NO consulta ***** ", none);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                String err = volleyError.toString();
                Log.i("No se pudo **********", err);
            }
        });

        //tiempo de respuesta, establece politica de reintentos
        request.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);
    }

}